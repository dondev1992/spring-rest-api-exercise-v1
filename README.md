# Project Setup

## Clone project

Use this URL to clone this project to your computer through your CLI or IDE

        https://gitlab.com/dondev1992/spring-rest-api-exercise-v1.git


        

## Starting Springboot API

1. Open the Spring REST API Exercise v1 project folder in your Intellij IDEA IDE.
2. Inside the project, open the EmployeeRunner.java file located in this path src/main/java/io/catalyte/training/springboot.
3. Click the green arrow in the left column next to the main method to start the Spring application. 
4. The server is now up and running

## Load 5 employees to server

1. First, install the Postman CLI to your computer.
2. Open your CLI on your computer, PowerShell for Windows or Terminal for Mac


    - ### **Windows installation**

    Run the following commands to install the Postman CLI for Windows. This will download an install script and run it. The install script creates a %USERPROFILE%\AppData\Local\Microsoft\WindowsApps directory if it doesn't exist yet, then installs a postman binary there.

        powershell.exe -NoProfile -InputFormat None -ExecutionPolicy AllSigned -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://dl-cli.pstmn.io/install/win64.ps1'))"



    - ### **Mac (Apple chip) installation**

    Run the following command to install the Postman CLI for Macs with Apple M1/M2 chips. This will download an install script and run it. The install script creates a /usr/local/bin directory if it doesn't exist yet, then installs a postman binary there. 

        curl -o- "https://dl-cli.pstmn.io/install/osx_arm64.sh" | sh



    - ### **Mac (Intel) installation**

    Run the following command to install the Postman CLI for Macs with Intel chips. This will download an install script and run it. The install script creates a /usr/local/bin directory if it doesn't exist yet, then installs a postman binary there.

        curl -o- "https://dl-cli.pstmn.io/install/osx_64.sh" | sh



3. Next, run this code in your CLI and this will run the Postman collection that will load 5 employee records onto the server

        postman login --with-api-key PMAK-63d4d2bf41ebf23df9e2384a-0b3318f0961808dc0aac7ea3b1bc0275c2

        postman collection run 18114134-3cf0fc69-1a73-4534-ac3b-06af4d5f70ad



4. Five employee records are now loaded on the Springboot API server.


## Postman collection folder

Click this button to access the Postman Collection
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/18114134-5a896fa6-0875-49b4-aee1-98f7eb366906?action=collection%2Ffork&collection-url=entityId%3D18114134-5a896fa6-0875-49b4-aee1-98f7eb366906%26entityType%3Dcollection%26workspaceId%3Dfcfe5c0a-a7f7-43f1-aa1f-db996c273308)


### Postman collection link

https://www.postman.com/orange-trinity-452585/workspace/springboot-exercise/collection/18114134-5a896fa6-0875-49b4-aee1-98f7eb366906?action=share&creator=18114134


### Postman Run acccess key: PMAK-63d4d2bf41ebf23df9e2384a-0b3318f0961808dc0aac7ea3b1bc0275c2