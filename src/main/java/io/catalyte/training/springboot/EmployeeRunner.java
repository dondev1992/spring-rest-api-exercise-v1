/**
 * @author - Donald Moore
 * @version - Spring REST API Exercise v1
 * @task - Setup and run Springboot API Web server application, load 5 records, use HTTP methods to execute CRUD functions
 */
package io.catalyte.training.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EmployeeRunner {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeRunner.class);

    }
}
