package io.catalyte.training.springboot.customExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Employee is not in the system.")
public class EmployeesNotFound extends RuntimeException{

}
