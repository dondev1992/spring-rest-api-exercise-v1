package io.catalyte.training.springboot.entities;


public class Employee {

    private int employeeId;
    private String firstName;
    private String lastName;
    private String title;
    private Boolean active;
    private Integer tenure;

    public Employee() {
    }

    public Employee(int employeeId, String firstName, String lastName, String title, Boolean active, Integer tenure) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.title = title;
        this.active = active;
        this.tenure = tenure;
    }

    public Employee(String firstName, String lastName, String title, Boolean active, Integer tenure) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.title = title;
        this.active = active;
        this.tenure = tenure;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getTenure() {
        return tenure;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", title='" + title + '\'' +
                ", active=" + active +
                ", tenure=" + tenure +
                '}';
    }
}
