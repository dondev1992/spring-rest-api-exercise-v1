package io.catalyte.training.springboot.controllers;

import io.catalyte.training.springboot.customExceptions.EmployeesNotFound;
import io.catalyte.training.springboot.entities.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {

    private final List<Employee> employees = new ArrayList<>();
    private int autoNumber = 0;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addEmployee(@RequestBody Employee employee) {
        autoNumber++;
        employee.setEmployeeId(autoNumber);
        employees.add(employee);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Employee> getEmployees(@RequestParam(required = false) Boolean active) {
        List<Employee> isActive = new ArrayList<>();
        List<Employee> notActive = new ArrayList<>();
        if (active != null) {
            if (active) {
                for (Employee employee : employees) {
                    if (employee.getActive()) {
                        isActive.add(employee);
                    }
                }
                return isActive;
            } else {
                for (Employee employee : employees) {
                    if (!employee.getActive()) {
                        notActive.add(employee);
                    }
                }
                return notActive;
            }
        }
        return employees;
    }

    @RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
    public Employee getEmployee(@PathVariable int employeeId) {
        for (Employee employee : employees) {
            if (employee.getEmployeeId() == employeeId) {
                return employee;
            }
        }
        throw new EmployeesNotFound();
    }

    @RequestMapping(value = "/{employeeId}", method = RequestMethod.PUT)
    public Employee updateEmployee(@PathVariable int employeeId, @RequestBody Employee employee) {
        for (Employee e : employees) {
            if (e.getEmployeeId() == employeeId) {
                int index = employees.indexOf(e);
                employees.set(index, employee);
                return employee;
            }
        }
        throw new EmployeesNotFound();
    }
    @RequestMapping(value = "/{employeeId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable int employeeId) {
        for (Employee employee : employees) {
            if (employee.getEmployeeId() == employeeId) {
                employees.remove(employee);
                return;
            }
        }

        throw new EmployeesNotFound();
    }

}
